import static org.junit.Assert.*;
import junit.framework.JUnit4TestAdapter;

import org.junit.Test;

/**
 * @author ben.ong
 * @created Jun 20, 2011
 */
public class DecoderTest {
	public static junit.framework.Test suite() {
		 return new JUnit4TestAdapter(DecoderTest.class);
	}
	
	/**
	 * Test method for {@link Decoder#decodeYearToInt(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testDecodeYearToInt(){
		String s = "FY11";
		assertEquals(2011, Decoder.decodeYearToInt(s));
	}
	
	/**
	 * Test method for {@link Decoder#decodeIntToYear(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testDecodeIntToYear(){
		int i = 2011;
		assertEquals("FY11", Decoder.decodeIntToYear(i));
		
		i = 112;
		assertEquals("FY12", Decoder.decodeIntToYear(i));

		i = 13;
		assertEquals("FY13", Decoder.decodeIntToYear(i));

		i = 4;
		assertEquals("FY04", Decoder.decodeIntToYear(i));
	}
	
	/**
	 * Test method for {@link Decoder#decodeDiscAROtoUndiscARO(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testDecodeDiscAROtoUndiscARO(){
		String s = "ARO_DISC1";
		assertEquals("ARO_UNDISC1", Decoder.decodeDiscAROtoUndiscARO(s));
		
		s = "ARO_DISC2";
		assertEquals("ARO_UNDISC2", Decoder.decodeDiscAROtoUndiscARO(s));
	}
	
	/**
	 * Test method for {@link Decoder#decodeAROAccretionToARODisc(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testDecodeAROAccretionToARODisc(){
		String s = "ARO_ACCRETION1";
		assertEquals("ARO_DISC1", Decoder.decodeAROAccretionToARODisc(s));
		
		s = "ARO_ACCRETION2";
		assertEquals("ARO_DISC2", Decoder.decodeAROAccretionToARODisc(s));
	}
	
	/**
	 * Test method for {@link Decoder#decodeReplace(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testDecodeReplace(){
		String memberName = "ARO_ACCRETION1";
		String pattern = "ACCRETION";
		String replacement = "DISC";
		
		assertEquals("ARO_DISC1", Decoder.decodeReplace(memberName, pattern, replacement));
		
		memberName = "ARO_DISC1";
		pattern = "DISC";
		replacement = "UNDISC";		
		assertEquals("ARO_UNDISC1", Decoder.decodeReplace(memberName, pattern, replacement));
	}
}
