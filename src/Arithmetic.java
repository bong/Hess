/**
 * 
 */

/**
 * @author ben.ong
 * @created Jul 14, 201l 
 */
public class Arithmetic {
	/**
	 * Adds a given integer to a "Year" dimension member.
	 * @param s - string in the form of "FY##"
	 * @param i - integer to add to year
	 * @return - the string "FYnn" where nn is the original years (##) plus i
	 */
	public static String addYear(String s, int i){
		s = s.substring(2);
		i += Integer.parseInt(s);
		String result = "";
		
		if( i < 10 )
			result = "0" + String.valueOf(i);
		else
			result = String.valueOf(i);
		
		return ("FY" + result);
	}
	
	/**
	 * Adds a given integer to a "PlanYear" dimension member.
	 * @param year - string in the form of "Plan##"
	 * @param i - integer to add to year
	 * @return - - the string "Plan##" where ## is the original years (##) plus i
	 */
	public static String addPlanYear(String year, int i){
		year = year.substring(4);
		i += Integer.parseInt(year);
		String result = "";
		
		if( i < 10 )
			result = "0" + String.valueOf(i);
		else
			result = String.valueOf(i);
		
		return ("Plan" + result);
	}
		
	/**
	 * Calculates the numeric difference between years in the "Years" dimension.
	 * @param yr1 - Year member name in the form of "FY##"
	 * @param yr2 - Year member name in the form of "FY##"
	 * @return - an integer value that is the difference between yr1 and yr2
	 */
	public static int diffYear(String yr1, String yr2){
		yr1 = yr1.substring(2);
		yr2 = yr2.substring(2);
		
		return Math.abs((Integer.parseInt(yr1) - Integer.parseInt(yr2)));
	}
}
