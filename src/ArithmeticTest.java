import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author ben.ong
 *
 */
public class ArithmeticTest {

	/**
	 * Test method for {@link Arithmetic#addYear(java.lang.String, int)}.
	 */
	@Test
	public void testAddYear() {
		String s = "FY11";
		
		int i = 2;		
		assertEquals("FY13", Arithmetic.addYear(s, i));
		
		i = -2;		
		assertEquals("FY09", Arithmetic.addYear(s, i));
	}
	
	/**
	 * Test method for {@link Arithmetic#addPlanYear(java.lang.String, int)}.
	 */
	@Test
	public void testAddPlanYear(){
		String planYear = "Plan2011";
		
		int i = 2;		
		assertEquals("Plan2013", Arithmetic.addPlanYear(planYear, i));
		
		i = -2;		
		assertEquals("Plan2009", Arithmetic.addPlanYear(planYear, i));
	}

	/**
	 * Test method for {@link Arithmetic#diffYear(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testDiffYear() {
		String s1 = "FY11";
		String s2 = "FY20";
		
		assertEquals(9, Arithmetic.diffYear(s1, s2));
		assertEquals(9, Arithmetic.diffYear(s2, s1));		
	}
}
