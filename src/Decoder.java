/**
 * @author ben.ong
 * @created Jun 20, 2011
 */
public class Decoder {
	/**
	 * 
	 * @param s - string in the form of "FY##"
	 * @return the year 20## as an integer
	 */
	public static int decodeYearToInt(String s){
		s = s.substring(2);
		return (2000 + Integer.parseInt(s));
	}
	
	public static String decodeIntToYear(int i){
		String s = String.valueOf(i);
		if(s.length() > 1)
			s = s.substring(s.length() - 2);
		else
			s = "0" + s;
		return "FY" + s;
	}
	
	public static String decodeDiscAROtoUndiscARO(String s){
		StringBuilder sb = new StringBuilder(s);
		sb.insert(4, "UN");
		
		return sb.toString();
	}
	
	public static String decodeAROAccretionToARODisc(String s){
		return s.replaceFirst("ACCRETION", "DISC");
	}
	
	public static String decodeReplace(String memberName, String pattern, String replacement){
		return memberName.replaceFirst(pattern, replacement);
	}
}
